package com.example.ivan.flashlite;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import java.security.Policy;


public class MainActivity extends AppCompatActivity {


    ImageButton btnSwitch;

    private Camera camera;
    private  boolean isFlashOn;
    private  boolean hasFlash;
    Parameters params;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //ENG: flash switch button
        //RUS: кнопка-переключатель фонарика
        btnSwitch = (ImageButton) findViewById(R.id.btnSwitch);

        btnSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isFlashOn) {
                    //ENG: turn off flash
                    //RUS: выключить вспышку
                    turnOffFlash();
                }
                else {
                    //ENG: turn on flash
                    //RUS: включить вспышку
                    turnOnFlash();
                }
            }
        });


        //RUS: getApplicationContext - Возвращает контекст глобального объекта.
        //     Обычно это необходимо, когда нужно получить контекст, жизненый цикл которого не связан с жизненным циклом приложения.
        //RUS: getPackageManager - Возвращает экземпляр PackageManager, чтобы найти информацию о глобальном пакете.
        //RUS: hasSystemFeature - Провереряет, является ли данное имя функции одной из доступных функций, возвращаемой функцией getSystemAvailableFeatures()
        //RUS: PackageManager.FEATURE_CAMERA_FLASH - включенная вспышка.
        hasFlash = getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        if(!hasFlash){
            //ENG: device doesn't support flash
            //ENG: Show alert message and close the application
            //RUS: устройство не поддерживает вспышку
            //RUS: Выводим сообщение о закрытии приложения

            AlertDialog alert = new AlertDialog.Builder(MainActivity.this).create();
            alert.setTitle("Error");
            alert.setMessage("Sorry, your device doesn't support flash light!");
            alert.setButton(Dialog.BUTTON_POSITIVE,"OK", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which) {
                    //ENG: closing the application
                    //RUS: закрываем приложение
                    finish();
                }
            });
            alert.show();
            return;
        }

    }


    //ENG: getting camera parameters
    //RUS: получение параметров камеры
    private void getCamera() {
        if(camera == null){
            try{
                camera = Camera.open();
                params = camera.getParameters();
            }
            catch (RuntimeException e){
                Log.e("Failed to Open. Error: ", e.getMessage());
            }
        }
    }

    //ENG: Turning On flash
    //RUS: Включение вспышки
    private void turnOnFlash(){
        if(!isFlashOn){
            if(camera == null || params == null){
                return;
            }
            params = camera.getParameters();
            params.setFlashMode(Parameters.FLASH_MODE_TORCH);
            camera.setParameters(params);
            camera.startPreview();
            isFlashOn = true;

            //ENG:  changing button/switch image
            //RUS:  изменения изображения кнопки/переключателя
            toggleButtonImage();
        }
    }

    //ENG: Turning Off flash
    //RUS: Отключение фонарика
    private void turnOffFlash(){
        if(isFlashOn){
            if(camera == null || params == null){
                return;
            }

            params = camera.getParameters();
            params.setFlashMode(Parameters.FLASH_MODE_OFF);
            camera.setParameters(params);
            camera.stopPreview();
            isFlashOn = false;

            toggleButtonImage();
        }
    }



    //ENG:
    //RUS:
    private  void toggleButtonImage(){
        if(isFlashOn){
            btnSwitch.setImageResource(R.drawable.btn_switch_on);
        }
        else{
            btnSwitch.setImageResource(R.drawable.btn_switch_off);
        }
    }

    @Override
    protected  void onDestroy(){
        super.onDestroy();
    }

    @Override
    protected void onPause(){
        super.onPause();

        //ENG: on pause turn off the flash
        //RUS: Пауза для выключенной вспышки
        turnOffFlash();
    }

    @Override
    protected void onResume(){
        super.onResume();

        //ENG: on resume turn on the flash
        //RUS: возобновление включения вспышки
        if(hasFlash)
            turnOnFlash();
    }

    @Override
    protected  void onStart(){
        super.onStart();

        //ENG: on starting the app get the camera params
        //RUS: при запуске приложения получаем параметры камеры
        getCamera();
    }

    @Override
    protected  void onStop(){
        super.onStop();

        //ENG: on stop release the camera
        //RUS: при остановке камеры
        if(camera != null){
            camera.release();
            camera = null;
        }
    }

}














